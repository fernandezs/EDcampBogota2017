<?php

namespace App\Http\Controllers;

use App\Models\Historical;
use Illuminate\Http\Request;

class HistoricalsController extends Controller
{
    public function index()
    {
        $historicals = Historical::with(['chapter', 'chapter.serie'])
            ->where('user_id', auth()->user()->id)
            ->orderBy('updated_at', 'desc')
            ->paginate();

        return view('users.historical', compact('historicals'));
    }
}
