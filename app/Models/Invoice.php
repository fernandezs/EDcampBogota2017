<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'invoice_date',
        'state',
    ];

    protected $dates = [
        'invoice_date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
