@extends('layouts.master')

@section('title')
    Home
@endsection

@section('header')
    @include('partials.header')
@endsection

@section('content')
    <main class="Home  u-afterFixed">
        <div class="Home-bgOpacity">
            <section class="Home-container">
                <article class="Home-mainContent">
                    <h1>SERIES, TUTORIALES, CURSOS</h1>
                    <p class="u-small">Donde quieras / Cuando quieras</p>
                    <br>
                    <a href="{{ route('explore') }}" class="u-button">Explorar series</a>
                </article>
            </section>
            <footer class="Home-footer">
                <p class="u-smaller">Únete a más de dos mil usuarios satisfechos en 2017</p>
            </footer>
        </div>
    </main>
@endsection
