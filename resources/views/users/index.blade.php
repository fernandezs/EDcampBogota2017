@extends('layouts.master')

@section('title')
    Usuarios
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Users u-afterFixed">
        <header class="Tables-title">
            <h2>Usuarios</h2>
        </header>
        @if($users->isEmpty())
            <header class="u-title">
                <h2>0 resultados</h2>
            </header>
        @else
            <div class="Table-container">
                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Estado</th>
                                <th>Rol</th>
                                <th>Acciones</th>
                            </tr>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->state }}</td>
                                <td>{{ $user->rol->name }}</td>
                                <td>
                                    <a href="{{ route('admin.users.edit', $user) }}">Editar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        <div class="text-center">
            {!! $users->links() !!}
        </div>
    </main>
@endsection
