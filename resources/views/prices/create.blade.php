@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Series  u-container  u-afterFixed">
        <header class="u-title">
            <h2>Precio nuevo</h2>
        </header>
        {!! Form::model(
            $price = new \App\Models\Price(),
            [
                'route' => 'admin.prices.store'
            ]
        ) !!}
            @include('prices.partials.form')
        {!! Form::close() !!}
    </main>
@endsection
