@extends('layouts.master')

@section('title')
    Recuperar contraseña
@endsection

@section('header')
    @include('partials.header')
@endsection

@section('content')
    <main class="Ingresar  u-afterFixed">
        <form id="Ingresar-form" method="POST" action="{{ route('password.email') }}" class="Form">
            <h2 class="Form-title">Recuperar contraseña</h2>
            {{ csrf_field() }}
            @if (session('status'))
                <div class="Form-message  u-success">
                    <strong>{{ session('status') }}</strong>
                </div>
            @endif
            <div class="Form-element">
                <label for="email"><i class="fa fa-envelope-o"></i></label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" required placeholder="Correo electrónico">
                @if ($errors->has('email'))
                    <div class="Form-message  u-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
            </div>
            <p class="Form-message  u-info  u-small">
                <strong>Le enviaremos una nueva contraseña por correo electrónico</strong>
            </p>
            <div class="Form-element  u-bg-success">
                <input type="submit" value="Recuperar contraseña">
            </div>
        </form>
        <aside class="Ingresar-registro">
            <p>
                ¿No tiene una cuenta?, <a href="{{ route('register') }}">regístrese</a>
            </p>
        </aside>
    </main>
@endsection
