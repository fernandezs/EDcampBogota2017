@extends('layouts.master')

@section('title')
    Ingresar
@endsection

@section('header')
    @include('partials.header')
@endsection

@section('content')
    <main class="Ingresar  u-afterFixed">
        <form id="Ingresar-form" method="POST" action="{{ route('login') }}" class="Form" >
            {{-- Campo obligatorio para todos los formularios que usen POST --}}
            {{ csrf_field() }}

            <h2 class="Form-title">Ingresar</h2>

            <div class="Form-element">
                <label for="email"><i class="fa fa-envelope-o"></i></label>
                <input type="email" name="email" id="email" placeholder="Correo electrónico">
            </div>
            {{-- Cuando hay un mensaje de validación del lado del servidor para el campo email --}}
            @if ($errors->has('email'))
                <div class="Form-message  u-error">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
            <div class="Form-element">
                <label for="password"><i class="fa fa-lock"></i></label>
                <input type="password" name="password" id="password" placeholder="Contraseña">
            </div>
            @if ($errors->has('password'))
                <div class="Form-message  u-error">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
            <div class="Form-element  u-bg-success">
                <input type="submit" value="Ingresar" class="u-bg-success">
            </div>
        </form>
        <aside class="Ingresar-menu">
            <p>
                <a href="{{ route('password.request') }}">¿Olvidó su contraseña?</a>
            </p>
            <p>
                ¿No tiene una cuenta?, <a href="{{ route('register') }}">regístrese</a>
            </p>
        </aside>
    </main>
@endsection
