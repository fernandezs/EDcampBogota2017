@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection


@section('content')
    <main class="Series  u-container  u-afterFixed">
        <header class="u-title">
            <h2>Serie nueva</h2>
        </header>
        {!! Form::model(
            $serie = new \App\Models\Serie(),
            [
                'route' => 'admin.series.store',
                'files' => 'true'
            ]
        ) !!}
            @include('series.partials.form')
        {!! Form::close() !!}
    </main>
@endsection
